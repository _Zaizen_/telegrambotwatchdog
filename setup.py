from distutils.core import setup
import py2exe, sys, os
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater
from pygame import mixer
import psutil
from telepot.namedtuple import ReplyKeyboardMarkup
import subprocess

sys.argv.append('py2exe')

setup(
    options = {'py2exe': {'bundle_files': 1, 'compressed': True}},
    windows = [{'script': "watchdog.pyw"}],
    zipfile = None,
)
