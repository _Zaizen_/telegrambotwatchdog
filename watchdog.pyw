from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater
from pygame import mixer
import psutil
import os
from telepot.namedtuple import ReplyKeyboardMarkup
import subprocess

keys = {}
keyboard = ReplyKeyboardMarkup(keyboard=[['KA', 'U'], ['KF', 'KAWR'], ['KIM', 'KITENS']])

#prende gli intenti e le chiavi di accesso alle API

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def load_config():
    with open(resource_path("keys.cfg")) as myfile:
        for line in myfile:
            name, var = line.partition("=")[::2]
            keys[name.strip()] = var[:-1]
        myfile.close()



def greenSquare():
    return u'\U00002705'
def redSquare():
    return u'\U0000274C'
def playGlitch():
    mixer.init()
    mixer.music.load('C:\dev\watchdog\sound.mp3')
    mixer.music.play()



def notifyTelegramPoint(update, context):
    #context.bot.send_message(chat_id=update.effective_chat.id, text='.')
    context.bot.send_message(chat_id=update.effective_chat.id, text=".")

def waitForInternetConnection():
    try:
        host = socket.gethostbyname("www.google.com")
        s = socket.create_connection((host, 80), 2)
        return True
    except:
        pass
    return False


def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    # Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;



def killFortnite(): #kill fortnite process
    if(fortniteRunning()):
        os.system("taskkill /f /im FortniteClient-Win64-Shipping.exe")
        
def killScelta(programma2):
        os.system("taskkill /f /im " + programma2)
        return
        
def GoScelta(programma1):
        os.system("start " + programma1)
        return

def fortniteRunning():
    return checkIfProcessRunning("FortniteClient-Win64-Shipping.exe")
    
def GoWarzone():
    if(warzoneRunning()):
        os.system("start ModernWarfare.exe")
        return
        
def GoChrome():
    if(warzoneRunning()):
        os.system("start chrome.exe")
        return
        
def GoExplorer():
    if(warzoneRunning()):
        os.system("start explorer.exe")
        return
        
def GoFortnite():
    if(warzoneRunning()):
        os.system("start FortniteClient-Win64-Shipping.exe")

def killFortniteLauncher(): #kill fortnite launcher process
    if(fortniteLauncherRunning()):
        os.system("taskkill /f /im EpicGamesLauncher.exe")

def fortniteLauncherRunning():
    return checkIfProcessRunning("EpicGamesLauncher.exe")

def killTelegram():
    if(telegramRunning()):
        os.system("taskkill /f /im Telegram.exe")
        
def explorerRunning():
    return checkIfProcessRunning("explorer.exe")

def telegramRunning():
    return checkIfProcessRunning("Telegram.exe")

def killDiscord():
    if(discordRunning()):
        os.system("taskkill /f /im Discord.exe")
        
def killExplorer():
    if(explorerRunning()):
        os.system("taskkill /f /im explorer.exe")

def discordRunning():
    return checkIfProcessRunning("Discord.exe")

def apexRunning():
    return checkIfProcessRunning("r5apex.exe")

def chromeRunning():
    return checkIfProcessRunning("chrome.exe")
    
def warzoneRunning():
    return checkIfProcessRunning("ModernWarfare.exe")

def killChrome():
    if(chromeRunning()):
        os.system("taskkill /f /im chrome.exe")
        
def killWarzone():
    if(warzoneRunning()):
        os.system("taskkill /f /im ModernWarfare.exe")

def killApex():
    if(apexRunning()):
        os.system("taskkill /f /im r5apex.exe")

def killAll():
    killFortniteLauncher()
    killFortnite()
    killTelegram()
    killDiscord()
    killApex()
    killChrome()

def updateUser(update, context):
    killTelegram() #prevent user seeing message on desktop

    if(fortniteLauncherRunning()):
        context.bot.send_message(chat_id=update.effective_chat.id, text="FL" + greenSquare())
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="FL" + redSquare())

    if (fortniteRunning()):
        context.bot.send_message(chat_id=update.effective_chat.id, text="F" + greenSquare())
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="F" + redSquare())

    if (discordRunning()):
        context.bot.send_message(chat_id=update.effective_chat.id, text="D" + greenSquare())
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="D" + redSquare())

    if(apexRunning()):
        context.bot.send_message(chat_id=update.effective_chat.id, text="A" + greenSquare())
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="A" + redSquare())



def shutdownPc():
    os.system('shutdown -s -t 0')

def message_handler(messaggio, update, context):
    
    programma=messaggio.split(" ", 1)
    text = programma[0].upper()
    #programma[1] = programma[1].replace("\\\\","\\")
    print(programma)
    print(text)
    if(text == 'KILL' or text == 'KILLALL' or text == 'KA'):
        killAll()
        notifyTelegramPoint(update, context)
    elif(text == 'KILLFORTNITE' or text == 'KF' or text == 'K'):
        killTelegram()
        killFortnite()
        killFortniteLauncher()
        notifyTelegramPoint(update, context)
    elif(text == 'UPDATE' or text == 'U'):
        updateUser(update, context)
    elif (text == '/START'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="Welcome back Master",parse_mode='HTML')
    elif(text == 'SHUTDOWN'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="Shutting down. Bye Bye")
        shutdownPc()
    elif(text == 'CHROME'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="viiiiiiaaaaaaa")
        killChrome()
        notifyTelegramPoint(update, context)
    elif(text == 'GOFORTNITE'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="giochiamo a fortnite")
        GoFortnite()
        notifyTelegramPoint(update, context)
    elif('KILLSCELTA' == text):
        context.bot.send_message(chat_id=update.effective_chat.id, text="killando " + programma[1])
        killScelta(programma[1])
        notifyTelegramPoint(update, context)
    elif('GOSCELTA' == text):
        context.bot.send_message(chat_id=update.effective_chat.id, text="avviando " + programma[1])
        GoScelta(programma[1])
        notifyTelegramPoint(update, context)
    elif(text == 'GOCHROME'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="giochiamo a fortnite")
        GoChrome()
        notifyTelegramPoint(update, context)
    elif(text == 'GOEXPLORER'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="giochiamo a fortnite")
        GoExplorer()
        notifyTelegramPoint(update, context)
    elif(text == 'GOWARZONE'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="giochiamo a fortnite")
        GoWarzone()
        notifyTelegramPoint(update, context)
    elif(text == 'GOALL'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="bro stai senza cuore")
        GoFortnite()
        GoWarzone()
        GoChrome()
        GoExplorer()
        notifyTelegramPoint(update, context)
    elif(text == 'GODOUBLEWARZONE'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="bro stai senza cuore e stai anche pazzo")
        GoWarzone()
        GoWarzone()
        notifyTelegramPoint(update, context)
    elif(text == 'EXPLORER'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="viiiiiiaaaaaaa e schermo nero")
        killExplorer()
        notifyTelegramPoint(update, context)
    elif(text == 'DISCORD'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="viiiiiiaaaaaaa e schermo nero")
        killDiscord()
        notifyTelegramPoint(update, context)
    elif(text == 'WARZONE'):
        context.bot.send_message(chat_id=update.effective_chat.id, text="viiiiiiaaaaaaaa")
        killWarzone()
        notifyTelegramPoint(update, context)
    elif(text == 'KILL ALL WITH REACTION' or text == 'KAWR'):
        killAll()
        notifyTelegramPoint(update, context)

        #context.bot.send_message(chat_id=update.effective_chat.id, text="Reaction still not implemented")
    elif(text == 'KIM'):
        time.sleep(60)
        killAll()
        notifyTelegramPoint(update, context)
    elif(text == 'KITENS'):
        time.sleep(10)
        killAll()
        notifyTelegramPoint(update, context)
    elif(text == 'S'):
        playGlitch()
        notifyTelegramPoint(update, context)
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="I don't understand...",parse_mode='HTML')





#def bot_telegram(session_id, ds, assistant):
def bot_telegram():
    updater = Updater(token=keys["Telegram_key"])
    #bot = telegram.Bot(token=keys["Telegram_key"])
    dispatcher = updater.dispatcher

    def start(update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text="Welcome back Master",parse_mode='HTML')

    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    def echo(update, context):
        messaggio = update.message.text

        print(messaggio)

        message_handler(messaggio, update, context)

    echo_handler = MessageHandler((~Filters.command), echo)
    dispatcher.add_handler(echo_handler)

    def unknown(update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text="Comando non riconosciuto")

    unknown_handler = MessageHandler(Filters.command, unknown)
    dispatcher.add_handler(unknown_handler)

    return updater


def quit(signo, _frame):
    print("Interrupted by %d, shutting down" % signo)
    #exit.set()

def start_everything():

    updater = bot_telegram()
    updater.start_polling()
    updater.idle()
    #return updater, assistant, session_id


def main():
    load_config()
    waitForInternetConnection()
    start_everything()

    print("Sessione chiusa!")


if __name__ == '__main__':

    main()


#sos
